#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_qtRenderdemo.h"

class qtRenderdemo : public QMainWindow
{
    Q_OBJECT

public:
    qtRenderdemo(QWidget *parent = Q_NULLPTR);

private:
    Ui::qtRenderdemoClass ui;
};
