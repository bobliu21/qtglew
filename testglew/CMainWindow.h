#pragma once

#include <QWidget>
#include <Windows.h>

#include <GL/glew.h>

#include "CDefEvent.h"

QT_BEGIN_NAMESPACE
class QEvent;
class QResizeEvent;
QT_END_NAMESPACE

class CMainWindow : public QWidget
{
	Q_OBJECT

public:
	CMainWindow(QWidget *parent = Q_NULLPTR);
	~CMainWindow();
	virtual QPaintEngine* paintEngine()const { return nullptr; }
protected:
	void OnInitDialog();
protected:
	virtual bool event(QEvent* event)override;
	virtual void resizeEvent(QResizeEvent* event)override;
	virtual void showEvent(QShowEvent* event)override;
private:
	bool CreateGLContent();
	void DefRenderer();
private:
	HDC m_hdc;
	HWND m_hwnd;
	HGLRC m_hrc;
};
