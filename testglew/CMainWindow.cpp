#include "CMainWindow.h"
#include "ui_CMainWindow.h"
#include <cassert>
#include <qmessagebox.h>
#include <QResizeEvent>
#include <QShowEvent>

#include <wingdi.h>


namespace Ui {
	class CMainWindow;
}
Ui::CMainWindow ui;

#pragma comment(lib,"opengl32")

CMainWindow::CMainWindow(QWidget *parent)
	: QWidget(parent,Qt::MSWindowsOwnDC)
	, m_hdc{}, m_hwnd{}, m_hrc{}
{
	//不需要qt绘制引擎，自己用opengl手动去实现绘制引擎
	setAttribute(Qt::WA_PaintOnScreen);		//设置此项需要自己实现qpaintengine方法。
	setAttribute(Qt::WA_NoSystemBackground);
	setAutoFillBackground(true);
	ui.setupUi(this);
	setWindowFlags(Qt::Window);
	OnInitDialog();
}

CMainWindow::~CMainWindow()
{
}

void CMainWindow::OnInitDialog()
{
	m_hwnd = (HWND)winId();
	CreateGLContent();
	wglMakeCurrent(m_hdc, m_hrc);
	assert(GLEW_OK == glewInit());

	QApplication::postEvent(this,new CDefEvent(CDefEvent::GL_Renderer));
}

bool CMainWindow::event(QEvent* event)
{
	if (CDefEvent::GL_Renderer == event->type())
		DefRenderer();
	return __super::event(event);
}

void CMainWindow::resizeEvent(QResizeEvent* event)
{
	glViewport(0,0,event->size().width(),event->size().height());
	QApplication::postEvent(this,new CDefEvent(CDefEvent::GL_Renderer));
	
}

void CMainWindow::showEvent(QShowEvent* event)
{
	QApplication::postEvent(this, new CDefEvent(CDefEvent::GL_Renderer));
}

bool CMainWindow::CreateGLContent()
{
	m_hdc = GetDC(m_hwnd);
	PIXELFORMATDESCRIPTOR _pfd{};
	ZeroMemory(&_pfd, sizeof(_pfd));
	_pfd.nSize = sizeof(_pfd);
	_pfd.nVersion = 1;
	_pfd.cColorBits = 32;
	_pfd.cDepthBits = 24;
	_pfd.cStencilBits = 8;
	_pfd.iPixelType = PFD_TYPE_RGBA;
	_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	int _format{0};
	_format = ChoosePixelFormat(m_hdc,&_pfd);
	if (0 == _format)
	{
		QMessageBox::information(this, "information", __func__);
		return false;
	}
	SetPixelFormat(m_hdc,_format,&_pfd);
	m_hrc = wglCreateContext(m_hdc);
	return true;
}

void CMainWindow::DefRenderer()
{
	glClearColor(0.0f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SwapBuffers(m_hdc);
}
