#include "CGlewWrapper.h"

CGlewWrapper::CGlewWrapper()
	:m_format{0}
	,m_hwnd{0},m_hdc{0},m_hrc{0}
{
}

CGlewWrapper::~CGlewWrapper()
{
	ShutDown();
}

bool CGlewWrapper::SetUp(HWND hwnd, HDC hdc)
{
	m_hwnd = hwnd;
	m_hdc = hdc;
	unsigned _pixelformat{};
	PIXELFORMATDESCRIPTOR _pfd={ sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER
		,PFD_TYPE_RGBA,32,0,0,0,0,0,0,0,0,0,0,0,0,0,24,8,0,PFD_MAIN_PLANE
		,0,0,0,0 };
	if (0 == m_format)
		_pixelformat = ChoosePixelFormat(m_hdc, &_pfd);
	else
		_pixelformat = m_format;

	if (!SetPixelFormat(m_hdc, _pixelformat, &_pfd))
		return false;
	//m_hrc = wglCreateContext(m_hdc);
#if 0
	if (!wglMakeCurrent(m_hdc, m_hrc))
		return false;
#endif // 0

	return true;
}

void CGlewWrapper::ShutDown()
{
}

void CGlewWrapper::SwapBuffer()
{
}
