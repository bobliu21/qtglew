#pragma once

#include <QEvent>

class CDefEvent : public QEvent
{
public:
	enum Type {
		GL_Renderer = QEvent::User + 1
	};
	CDefEvent(CDefEvent::Type type);
	~CDefEvent();

};
