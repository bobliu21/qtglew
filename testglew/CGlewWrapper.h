#pragma once

#include <Windows.h>
#include <GL/glew.h>

#pragma comment(lib,"glew32")



class CGlewWrapper
{
public:
	CGlewWrapper();
	~CGlewWrapper();
	
	bool SetUp(HWND hwnd, HDC hdc);
	void ShutDown();
	void SwapBuffer();
private:
	int m_format;
	HWND m_hwnd;
	HDC m_hdc;
	HGLRC m_hrc;
};

